/*
 * Copyright 2016 <Admobilize>
 * MATRIX Labs  [http://creator.matrix.one]
 * This file is part of MATRIX Creator HAL
 *
 * MATRIX Creator HAL is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <iostream>

#include "../cpp/driver/everloop_image.h"
#include "../cpp/driver/everloop.h"
#include "../cpp/driver/imu_data.h"
#include "../cpp/driver/imu_sensor.h"
#include "../cpp/driver/gpio_control.h"
#include "../cpp/driver/wishbone_bus.h"

namespace hal = matrix_hal;
#define INPUT 0
#define OUTPUT 1

#define ZERO 0
#define ONE 1
#define PIN_0 0
#define PIN_1 1
#define PIN_LAMP 3

#define CLK_FRQ 200000000

// Prototipes
unsigned char getBit(unsigned int);

int main(int argc, const char** argv){
  hal::WishboneBus bus;
  bus.SpiInit();
  hal::GPIOControl gpio;
  gpio.Setup(&bus);
  unsigned char inputPinList[8] = {0, 2, 4, 6, 8, 10, 12, 14};
  unsigned char outputPinList[8] = {1, 3, 5, 7, 9, 11, 13, 15};
  unsigned char option = atoi(argv[1]);

  gpio.SetMode(inputPinList, sizeof(inputPinList), INPUT);
  gpio.SetMode(outputPinList, sizeof(outputPinList), OUTPUT);

  uint16_t write_data = 1;
  gpio.SetGPIOValues(outputPinList, sizeof(outputPinList), write_data);
  uint16_t feed = 0;
  unsigned char turn;

  std::system("clear");

  while (true){
   switch(option){
    case 2:
     gpio.SetGPIOValue(PIN_1, ONE);
     while(!turn){
      feed = gpio.GetGPIOValue(PIN_0);
      if(feed){
        usleep(3000000);
        turn = 0xFF;
        gpio.SetGPIOValue(PIN_1, ZERO);
      }
     }
    break;
    case 3:
       gpio.SetGPIOValue(PIN_LAMP, ONE);
       usleep(1000000);
       gpio.SetGPIOValue(PIN_LAMP, ZERO);
    break;
    default:
    std::cout << "Default" << std::endl;
    break;
   }
   return 0;
  }
  return 0;
}

unsigned char getBit(unsigned int value){
 return value & 0x01;;
}