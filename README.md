# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This proyect uses a RPI 3B+ and a MATRIX Creator board to automate an aquarium needs like feed, temperature measurements, toggle lighting and PH readings.

Version 0.1 - Date: 10/2017

This is a proyect for Embedded Systems class in the National University of Colombia

### How do I get set up? ###

Hardware:

- RPI 3B+
- MATRIX Creator board
- RPI Camera
- OneWire Sensor (DS18B20 was used here)
- One level shifter (3.3V to 5V)
- Two 5V regulators (0.5A and 2A)

Software:

First of all you need to get and mount a working Raspbian distribution for Raspberry PI 3B+, we used RASPBIAN STRETCH WITH DESKTOP because the need for a graphic visualization because of the PH sensor, also you'll need to install OpenCV in to your RPI, finally into your RPI you'll need to install Apache in order to get your own web site via lamp functionality.

### Who do I talk to? ###

Developer: David Tosse, Electronic Engineer Student