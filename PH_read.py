#!/usr/bin/env python
# -*- coding: utf-8 -*-

#--------- libraries ---------#
from picamera import PiCamera #
from time import sleep        #
from subprocess import call   #
import cv2                    #
import cv2.cv as cv           #
import numpy as np            #
import os.path                #
import time                   #
import sys                    #
#-----------------------------#

#----------- variables ------------#
hour = time.strftime("%H")         #
day = time.strftime("%d")          #
month = time.strftime("%m")        #
year = time.strftime("%Y")         #
date = "/home/pi/Documents/Camera_Projects/Data/"+day+month+year+'.csv'#
## external circle diameter in     #
## [pixels]                        #
extDim = 205                       #
## tolerance of external diameter  #
## circle                          #
tolerance = 10                     #
## inner circle dimension variable #
dim = 0                            #
## center position of original     #
## complete image                  #
x = 0                              #
y = 0                              #
## dimension of the scanned square #
smallSqr = 29                      #
## array of green pixel values     #
##for center                       #
arrayColorG = []                   #
## mean value of arrayColorG array #
centerG = []                       #
## PH green color values           #
PH_1 = 0                           #
PH_2 = 0                           #
PH_3 = 0                           #
PH_4 = 0                           #
PH_5 = 0                           #
PH_6 = 0                           #
PH_7 = 0                           #
PH_8 = 0                           #
## array with compared values of   #
## center color and ranges         #
dif = np.zeros(7)                  #
## minor difference value of       #
## dif array                       #
error = 0                          #
## position of the minimum error   #
## value in dif array              #
posError = 0                       #
## final value according with      #
##sensor                           #
ph_measure = 0                     #
## ph detecting success procedure  #
success = 0                        #
#----------------------------------#

##----- taking photo -----
call(["/home/pi/matrix-creator-quickstart/matrix-creator-hal/build/demos/arc_demo","100"])
while success == 0:
 camera = PiCamera()
 camera.capture('image.jpg')
 camera.close()
 ##-----loading image -----
 ## loading only blue component of the image in order to avoid
 ## noise colors
 img = cv2.imread('image.jpg',0)
 ##blur process to reduce noise
 img = cv2.medianBlur(img,5)
 ##image to gray scale to reduce noise
 cimg = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
 """opencv function that detects circles
 Parameters:
 - input image
 - detection method
 - inverse ration of resolution
 - minimim distance between centers"""
 circles = cv2.HoughCircles(img, cv.CV_HOUGH_GRADIENT, 2, 50)
 ##loading complete original image
 img = cv2.imread('image.jpg')
 ## drawing circles
 for i in circles[0,:]:
  ## tolerance criteria
  if(i[2] < extDim + tolerance):
   if(i[2] > extDim - tolerance):
    ## assign variables values
    dim = int(round(i[2]/3.3))
    centerPos = ((extDim+1)-dim)/2
    x = i[0]
    y = i[1]
    ## draw the outer circle
    #cv2.circle(img,(i[0],i[1]),i[2],(0,255,0),1)
    ## draw the center of the circle
    #cv2.circle(img,(i[0],i[1]),2,(0,0,255),3)
    ## draw inner sensor circle
    #cv2.circle(img,(i[0],i[1]), dim, (255,0,0),1)
    print("Success")
    success = 1
   else: # catch *all* exceptions
    sleep(1)
    print("Error")

call(["/home/pi/matrix-creator-quickstart/matrix-creator-hal/build/demos/arc_demo","0"])
#cropping image to analize only the sensor area
cropImg = img[y-extDim:y+extDim, x-extDim:x+extDim]
##HSV transformation (optional but workless)
#hsv = cv2.cvtColor(cropImg, cv2.COLOR_BGR2HSV)
##center color detection
for k in range(0, dim*dim):
 i = k/dim
 j = k-dim*(i)
 arrayColorG.append(k)
##filling centerColor array
 arrayColorG[k] = cropImg[extDim-dim/2+i][extDim-dim/2+j][1]
##painting the scanned area with black (to check)
#cropImg[extDim-dim/2+i][extDim-dim/2+j] = [0,0,0]
## computing mean value of the array
centerG = np.mean(arrayColorG)
## print the center mean detected value
#print centerG
## function that empty arrayColorG
def zerosArray():
 arrayColorG[:] = []
##----- Sensor circles color detection (PH has eight circles) -----
## First color detection
## drawing a rectagle to see the area to be scanned
#cv2.rectangle(cropImg, (extDim-55,extDim+88), (extDim-25,extDim+118), (0, 255, 0), 1)
zerosArray()
for k in range(0, smallSqr*smallSqr):
 i = k/smallSqr
 j = k-smallSqr*(i)
 arrayColorG.append(k)
 arrayColorG[k] = cropImg[extDim+88+i][extDim-50+j][1]
## painting the scanned area with black (to check)
#cropImg[extDim+88+i][extDim-50+j] = [0, 0, 0]
## computing mean value of the array
PH_1 = np.mean(arrayColorG)
## Second color detection
## drawing a rectagle to see the area to be scanned
#cv2.rectangle(cropImg, (extDim-118,extDim+28), (extDim-88,extDim+58), (0, 255, 0), 1)
zerosArray()
for k in range(0, smallSqr*smallSqr):
 i = k/smallSqr
 j = k-smallSqr*(i)
 arrayColorG.append(k)
 arrayColorG[k] = cropImg[extDim+25+i][extDim-110+j][1]
## painting the scanned area with black (to check)
#cropImg[extDim+25+i][extDim-110+j] = [0, 0, 0]
## computing mean value of the array
PH_2 = np.mean(arrayColorG)
## Third color detection
## drawing a rectagle to see the area to be scanned
#cv2.rectangle(cropImg, (extDim-118,extDim-28), (extDim-88,extDim-58), (0, 255, 0), 1)
zerosArray()
for k in range(0, smallSqr*smallSqr):
 i = k/smallSqr
 j = k-smallSqr*(i)
 arrayColorG.append(k)
 arrayColorG[k] = cropImg[extDim-55+i][extDim-110+j][1]
## painting the scanned area with black (to check)
#cropImg[extDim-55+i][extDim-110+j] = [0, 0 , 0]
## computing mean value of the array
PH_3 = np.mean(arrayColorG)
## Fourth color detection
## drawing a rectagle to see the area to be scanned
#cv2.rectangle(cropImg, (extDim-25,extDim-120), (extDim-55,extDim-90), (0, 255, 0), 1)
zerosArray()
for k in range(0, smallSqr*smallSqr):
 i = k/smallSqr
 j = k-smallSqr*(i)
 arrayColorG.append(k)
 arrayColorG[k] = cropImg[extDim-118+i][extDim-43+j][1]
## painting the scanned area with black (to check)
#cropImg[extDim-118+i][extDim-43+j] = [0, 0, 0]
## computing mean value of the array
PH_4 = np.mean(arrayColorG)
## Fifth color detection
## drawing a rectagle to see the area to be scanned
#cv2.rectangle(cropImg, (extDim+25,extDim-120), (extDim+55,extDim-90), (0, 0, 0), 1)
zerosArray()
for k in range(0, smallSqr*smallSqr):
 i = k/smallSqr
 j = k-smallSqr*(i)
 arrayColorG.append(k)
 arrayColorG[k] = cropImg[extDim-118+i][extDim+28+j][1]
## painting the scanned area with black (to check)
#cropImg[extDim-118+i][extDim+28+j] = [0, 0, 0]
## computing mean value of the array
PH_5 = np.mean(arrayColorG)
## Sixth color detection
## drawing a rectagle to see the area to be scanned
#cv2.rectangle(cropImg, (extDim+88,extDim-28), (extDim+118,extDim-58), (0, 0, 0), 1)
zerosArray()
for k in range(0, smallSqr*smallSqr):
 i = k/smallSqr
 j = k-smallSqr*(i)
 arrayColorG.append(k)
 arrayColorG[k] = cropImg[extDim-55+i][extDim+88+j][1]
## painting the scanned area with black (to check)
#cropImg[extDim-55+i][extDim+88+j] = [0, 0, 0]
## computing mean value of the array
PH_6 = np.mean(arrayColorG)
## Seventh color detection
## drawing a rectagle to see the area to be scanned
#cv2.rectangle(cropImg, (extDim+88,extDim+28), (extDim+118,extDim+58), (0, 255, 0), 1)
zerosArray()
for k in range(0, smallSqr*smallSqr):
 i = k/smallSqr
 j = k-smallSqr*(i)
 arrayColorG.append(k)
 arrayColorG[k] = cropImg[extDim+88+i][extDim+28+j][1]
## painting the scanned area with black (to check)
#cropImg[extDim+25+i][extDim+88+j] = [0, 0, 0]
## computing mean value of the array
PH_7 = np.mean(arrayColorG)
## Eighth color detection
## drawing a rectagle to see the area to be scanned
#cv2.rectangle(cropImg, (extDim+25,extDim+88), (extDim+55,extDim+118), (0, 255, 0), 1)
zerosArray()
for k in range(0, smallSqr*smallSqr):
 i = k/smallSqr
 j = k-smallSqr*(i)
 arrayColorG.append(k)
 arrayColorG[k] = cropImg[extDim+88+i][extDim+28+j][1]
## painting the scanned area with black (to check)
#cropImg[extDim+88+i][extDim+28+j] = [0, 0, 0]
## computing mean value of the array
PH_8 = np.mean(arrayColorG)
## printing PHs values (to check)
#print PH_1, "\n", PH_2, "\n", PH_3, "\n", PH_4, "\n", PH_5, "\n", PH_6, "\n", PH_7,  "\n",PH_8
## -----determining sensor PH value -----
## computing differences
dif[0] = abs(PH_1 - centerG)
dif[1] = abs(PH_2 - centerG)
dif[2] = abs(PH_3 - centerG)
dif[3] = abs(PH_4 - centerG)
dif[4] = abs(PH_5 - centerG)
dif[5] = abs(PH_6 - centerG)
dif[6] = abs(PH_7 - centerG)
## printing dif array (to check)
#print dif
if centerG < 40:
 posError = 8
else:
 error = min(dif)
 for i in range(0, len(dif)):
  if dif[i] == error:
   posError = i
ph_measure = 5.8 + posError*0.4
if posError == 6:
 ph_measure = 8
if centerG < 40:
 ph_measure = 8.2
#---------- Saving image ---------#
cv2.imwrite('photo.jpg', cropImg) #
#---------------------------------#
#------- checking file ------#
if os.path.exists(date):     #
 file = open(date,'a')       #
else:                        #
 file = open(date,'w+')      #
 file.write('hour,ph')       #
 file.write('\n')            #
#----------------------------#
#------ inserting data ------#
if hour == 0:                #
 hour = '0'                  #
file.write(hour)             #
file.write(',')              #
file.write(str(ph_measure))  #
file.write('\n')             #
file.close()                 #
#----------------------------#

## showing cropped image
#cv2.imshow('Sensor',cropImg)
#cv2.waitKey(0)
#cv2.destroyAllWindows()